#!/usr/bin/env perl

use Modern::Perl '2011';
use autodie;

use Getopt::Euclid qw(:vars);
use Smart::Comments;

use LWP::Simple qw(get);
use List::UtilsBy qw(max_by);
use POSIX qw (ceil); 
use Data::UUID;
use XML::Bare;
use Template;

use Bio::MUST::Core;
use aliased 'Bio::MUST::Core::Taxonomy';

#TODO is it a good solution to modify user option est to 0 when no transcript (not too confusing?) or use thing such as rna_launch?

 
# Paths of the dependencies and required software packages
# To use AMAW script in its stand alone version, please adapt the following
# software package paths and calls to your dependency environment
my $maker = 'maker';
my $fasta_merge = 'fasta_merge';
my $gff3_merge = 'gff3_merge';
my $repeatmasker = '/usr/local/RepeatMasker/RepeatMasker';
my $maskingDB = '/usr/local/maker/data/te_proteins.fasta'; # transposable element protein for RepeatRunner
my $exonerate = '/usr/local/exonerate-2.2.0-x86_64/bin/exonerate';

# BLAST tool paths
my $blastn = '/usr/local/ncbi-blast-2.10.0+/bin/blastn';
my $blastx = '/usr/local/ncbi-blast-2.10.0+/bin/blastx';
my $tblastx = '/usr/local/ncbi-blast-2.10.0+/bin/tblastx';
my $makeblastdb = '/usr/local/ncbi-blast-2.10.0+/bin/makeblastdb';

# Predictor tool paths
# 1 - SNAP
my $snap = '/usr/local/SNAP/snap';
my $fathom = 'fathom';
my $forge = 'forge';
my $hmm_assembler = 'hmm-assembler.pl';
my $maker2zff = 'maker2zff';

# 2 - Augustus
my $augustus_dir = '/usr/local/augustus';
my $augustus = $augustus_dir . '/bin/augustus';
my $augustus_new_species = $augustus_dir . '/scripts/new_species.pl';
my $augustus_randomsplit = $augustus_dir . '/scripts/randomSplit.pl';
my $convert_fathom2genbank = '/usr/local/convert_fathom2genbank.pl';
my $augustus_config = '/mnt/augustus-config/'; # Path to config directory containing augustus gene models, usually config/ directory in augustus main folder
my $etraining = 'etraining';

# Other software packages
my $perl = 'perl';
my $fastq_dump = 'fastq-dump';
my $trinity = 'Trinity';
my $trinity_rsem = '/usr/local/trinityrnaseq-Trinity-v2.4.0/util/align_and_estimate_abundance.pl';
my $transcript_filter = '/usr/local/transcript-filter.pl';    # Homemade script provided with AMAW

# Only needed to parallelize jobs with Open MPI
my $mpiexec = 'mpiexec';
my $openmpi_ldpreload = '/usr/lib64/openmpi/lib/libmpi.so:$LD_PRELOAD'; 

# Evidence data paths
my $default_protein_db_folder = '/usr/local/prot_dbs/';


# AMAW script
# define global env variable
system  "export AUGUSTUS_CONFIG_PATH=$augustus_config";

# summary of parameters values
say '# Summary of parameter values:';
map{ say $_ =~ s/^--//r . ' = ' . $ARGV{$_} } sort keys %ARGV;

# work directory creation
mkdir $ARGV_outdir unless -d $ARGV_outdir;
chdir $ARGV_outdir;

# jobname building
my $ug = Data::UUID->new;
my $uuid = $ug->create_str;
$ARGV_jobname = $ARGV_organism . '-' . $uuid unless $ARGV_jobname;

if ($ARGV{'--transcript-db'}) {

    unless ($ARGV{'--est-file'}) {
        
        opendir(DIR, $ARGV_transcript_db);
        my @filenames = grep{ /\.fasta|\.fa|\.fna/ } readdir(DIR);
        
        #/!\ careful because if --est-file option is not defined in command line, $ARGV{'--est-file'} and $ARGV_est_file can have two different values
        my ($transcript_file) = grep{ /$ARGV_organism/ } @filenames;
        
        #TODO add robustness to path -> check the ...'/'... 
        if ($transcript_file) {
            $ARGV_est_file = $ARGV_transcript_db . $transcript_file;
            say $ARGV_est_file. ' transcript file matched for your organism, '
            . 'it will be used as evidence for annotation'; 
        }

        else {

            say 'No transcript file matching for your organism, the SRA '
            . 'search will be executed';

        }
        
    }

    else {

        say '--transcript-db option disabled, you can not use this option '
        . 'if you select a file with --est-file option.'

    }
}

# determine which protein bank to use when protein evidence is activated and no user file is given
if ($ARGV{'--proteins'} && not defined $ARGV{'--protein-file'}) {

    my $tax = Taxonomy->new(
        tax_dir   => $ARGV_taxdir,
        save_mem  => 1,
        fuzzy_map => 1,
    );

    my $taxid = $tax->get_taxid_from_seq_id($ARGV_organism . '@1');
    my $phylum = ( $tax->get_taxonomy($taxid) )[2];

    #TODO associate protein db for maker control files
    if ($ARGV{'--prot-dbs'}) { 
        $ARGV_protein_file = $ARGV{'--prot-dbs'}
        . $phylum . '_id99.faa';
    } 
    else {
        $ARGV_protein_file = '/usr/local/prot_dbs/'
        . $phylum . '_id99.faa';
    }
}

# setup of transcript de novo building when est evidence is activated and no user file is given
my $rna_launch;
if ($ARGV{'--est'} && not defined $ARGV_est_file) {
    $rna_launch = 1;
    transcript_builder();
    $ARGV_est_file = 'Trinity_filtered.fasta';
}

### Creation of the job files for the different steps of the pipeline 
maker_templating();
maker_runs();

### Launching of the jobs

my $parallelization_mode = $ARGV_mpi ? 'orte ' : 'snode ';

# Assemble RNA-Seq transcripts from NCBI SRA
if ($ARGV{'--est'} && $rna_launch) {

    if ($ARGV_sge) {
        system 'qsub fastq-dump.sh';

        my $trinity_cmd = 'qsub -pe snode ' . $ARGV_trinity_cpus . ' -hold_jid '
            . $ARGV_jobname . '_fastq-dump trinity.sh';
        system $trinity_cmd;

        my $rsem_cmd =    'qsub -pe snode ' . $ARGV_rsem_cpus . ' -hold_jid '
            . $ARGV_jobname . '_trinity rsem.sh';
        system $rsem_cmd;

        my $filter_cmd =  'qsub -hold_jid ' . $ARGV_jobname
            . '_rsem transcript_filter.sh';
        system $filter_cmd;
    }

    else { 
        system 'sh fastq-dump.sh';

        my $trinity_cmd = 'sh trinity.sh';
        system $trinity_cmd;

        my $rsem_cmd =    'sh rsem.sh';
        system $rsem_cmd;

        my $filter_cmd =  'sh transcript_filter.sh';
        system $filter_cmd;
    }
}

# Launch MAKER runs
if ($ARGV{'--gm-db'} && -e $ARGV_snap_db . $ARGV_organism . '.hmm'
    && -e $ARGV_augustus_db . $ARGV_augustus_prefix . $ARGV_organism) {

    if ($ARGV_sge) {
        my $maker_cmd =  'qsub -pe ' . $parallelization_mode
            . $ARGV{'--maker-cpus'} . ' maker_run.sh';
        system $maker_cmd;
    }

    else {
        my $maker_cmd =  'sh maker_run.sh';
        system $maker_cmd;
    }

}

elsif ($ARGV_augustus_gm) {

    if ($ARGV_sge) {
        my $maker_cmd =  'qsub -pe ' . $parallelization_mode
            . $ARGV{'--maker-cpus'} . ' maker_run.sh';
        system $maker_cmd;
    }

    else {
        my $maker_cmd =  'sh maker_run.sh';
        system $maker_cmd;
    }

}

else {

    if ($ARGV_sge) {

        my $maker1_cmd = 'qsub -pe ' . $parallelization_mode
            .  $ARGV{'--maker-cpus'} . ' maker_run1.sh';

        system $maker1_cmd;

        my $maker2_cmd = 'qsub -pe ' . $parallelization_mode. $ARGV_maker_cpus
            . ' -hold_jid ' . $ARGV_jobname . '_maker_run1 maker_run2.sh';
        system $maker2_cmd;   

        my $maker3_cmd = 'qsub -pe ' . $parallelization_mode . $ARGV_maker_cpus
            . ' -hold_jid ' . $ARGV_jobname . '_maker_run2 maker_run3.sh';
        system $maker3_cmd;
    }

    else { 

        my $maker1_cmd = 'sh maker_run1.sh';

        system $maker1_cmd;

        my $maker2_cmd = 'sh maker_run2.sh';
        system $maker2_cmd;   

        my $maker3_cmd = 'sh maker_run3.sh';
        system $maker3_cmd;

    }
}
 
# ### %ARGV

sub maker_runs {

    my $run1_tt = <<'EOT';
#!/bin/bash
#$ -V
#$ -cwd
#$ -q [% queue %]
#$ -m [% IF mail %]beas[% ELSE %]n[% END %]
[%- IF mail %]#$ -M [% mail %][% END %]
#$ -N [% jobname %]_maker_run1
 
# maker first run - evidence-driven gene prediction
[%- IF mpi %]
export LD_PRELOAD=[% openmpi_ldpreload %]
[% mpiexec %] -n [% maker_cpus %] [% maker %] -fix_nucleotides -base run1
[% ELSE %]
[% maker %] -fix_nucleotides -base run1
[%- END -%]

# creation of directories where result extractions  will be stored
mkdir output_run1/ output_run2/ final_output/
mkdir -p gene_models/{run1,run2}/{snap,genemark,augustus}/

# extraction of maker results 
[% fasta_merge %] -d ./run1.maker.output/*master_datastore_index.log -o run1
[% gff3_merge %] -d ./run1.maker.output/*master_datastore_index.log -o run1_fasta.gff # GFF including fasta seqiences for snap training with maker tools
[% gff3_merge %] -n -d ./run1.maker.output/*master_datastore_index.log -o run1.gff # -n is for avoid getting fasta in footer -> augustus and genemarker training
mv -t output_run1/ run1*fasta run1*gff

# SNAP training
cd gene_models/run1/snap/
[% IF transcripts == '' %][% maker2zff %] -n ../../../output_run1/run1_fasta.gff
[% ELSE %][% maker2zff %] ../../../output_run1/run1_fasta.gff[% END %]
[% fathom %] -categorize 1000 genome.ann genome.dna
[% fathom %] -export 1000 -plus uni.ann uni.dna
[% forge %] export.ann export.dna
[% hmm_assembler %] snap . > snap.hmm

cd ../../../

# replace maker options run1 by run2
mv maker_opts_run2.ctl maker_opts.ctl
EOT

    # TODO option to delete previous augustus gene model
    my $run2_tt = <<'EOT';
#!/bin/bash
#$ -V
#$ -cwd
#$ -q [% queue %]
#$ -m [% IF mail %]beas[% ELSE %]n[% END %]
[%- IF mail %]#$ -M [% mail %][% END %]
#$ -N [% jobname %]_maker_run2

# maker second run - evidence-driven gene predictions
[%- IF mpi %]
export LD_PRELOAD=[% openmpi_ldpreload %]
[% mpiexec %] -n [% maker_cpus %] [% maker %] -fix_nucleotides -R -base run2
[%- ELSE %]
[% maker %] -fix_nucleotides -R -base run2
[%- END %]

# extraction of maker results
[% fasta_merge %] -d ./run2.maker.output/*master_datastore_index.log -o run2
[% gff3_merge %] -d ./run2.maker.output/*master_datastore_index.log -o run2_fasta.gff # GFF including fasta seqiences for snap training with maker tools
[% gff3_merge %] -n -d ./run2.maker.output/*master_datastore_index.log -o run2.gff # -n is for avoid getting fasta in footer -> augustus and genemarker training

mv -t output_run2/ run2*fasta run2*gff

# SNAP training
cd gene_models/run2/snap/
[% IF transcripts == '' %][% maker2zff %] -n ../../../output_run2/run2_fasta.gff
[% ELSE %][% maker2zff %] ../../../output_run2/run2_fasta.gff[% END %]
[% fathom %] -categorize 1000 genome.ann genome.dna
[% fathom %] -export 1000 -plus uni.ann uni.dna
[% forge %] export.ann export.dna
[% hmm_assembler %] snap . > snap.hmm

# Augustus training
cd ../augustus/
[% augustus_new_species %] --species=[% augustus_gm %]
[% convert_fathom2genbank %] ../snap/uni.ann ../snap/uni.dna all > genes.gb  # script provided by MAKER developers, all can be replaced by a number of gene selected 
#convert from fathom let the advantage to choose the number of genes (but how does it choose?)
[% augustus_randomsplit %] genes.gb 100
[% etraining %] --species=[% augustus_gm %] genes.gb.train
[% augustus %] --species=[% augustus_gm %] genes.gb.test  > training.log
cd ../../../

# replace maker options run2 by run3
mv maker_opts_run3.ctl maker_opts.ctl
EOT

    my $run3_tt = <<'EOT';
#!/bin/bash
#$ -V
#$ -cwd
#$ -q [% queue %]
#$ -m [% IF mail %]beas[% ELSE %]n[% END %]
[%- IF mail %]#$ -M [% mail %][% END %]
#$ -N [% jobname %]_maker_run3

# maker second run - evidence-driven gene predictions
[% IF mpi %]
export LD_PRELOAD=[% openmpi_ldpreload %]
[% mpiexec %] -n [% maker_cpus %] [% maker %] -fix_nucleotides -R -base run3
[% ELSE %]
[% maker %] -fix_nucleotides -R -base run3
[% END %]


# extraction of the final maker results
[% fasta_merge %] -d ./run3.maker.output/*master_datastore_index.log -o run3
[% gff3_merge %] -d ./run3.maker.output/*master_datastore_index.log -o run3_fasta.gff # GFF including fasta seqiences for snap training with maker tools
[% gff3_merge %] -n -d ./run3.maker.output/*master_datastore_index.log -o run3.gff # -n is for avoid getting fasta in footer -> augustus and genemarker training
 
mv -t final_output/ run3*fasta run3*gff

EOT

    my $altrun_tt = <<'EOT';
#!/bin/bash
#$ -V
#$ -cwd
#$ -q [% queue %]
#$ -m [% IF mail %]beas[% ELSE %]n[% END %]
[%- IF mail %]#$ -M [% mail %][% END %]
#$ -N [% jobname %]_maker

# maker second run - evidence-driven gene predictions
[% IF mpi %]
export LD_PRELOAD=[% openmpi_ldpreload %]
[% mpiexec %] -n [% maker_cpus %] [% maker %] -fix_nucleotides -base run
[% ELSE %]
[% maker %] -fix_nucleotides -R -base run
[% END %]

# creation of directories where result extractions  will be stored
mkdir final_output/ 

[% fasta_merge %] -d ./*maker.output/*master_datastore_index.log -o run
[% gff3_merge %] -d ./*maker.output/*master_datastore_index.log -o run_fasta.gff # GFF including fasta seqiences for snap training with maker tools
[% gff3_merge %] -n -d ./*maker.output/*master_datastore_index.log -o run.gff # -n is for avoid getting fasta in footer -> augustus and genemarker training
mv -t final_outpt/ run*fasta run*gff
EOT

    my %vars = (
        augustus_gm   => $ARGV_augustus_prefix . $ARGV_organism,
        maker_cpus    => $ARGV_maker_cpus,
        queue         => $ARGV_queue,
        mail          => $ARGV_email, 
        jobname       => $ARGV_jobname,
        protein       => $ARGV_proteins,
        est           => $rna_launch,
        mpi           => $ARGV_mpi,
        maker         => $maker,
        fasta_merge   => $fasta_merge,
        gff3_merge    => $gff3_merge,
        fathom        => $fathom,
        forge         => $forge,
        hmm_assembler => $hmm_assembler,
        maker2zff     => $maker2zff,
        augustus      => $augustus,
        augustus_new_species => $augustus_new_species,
        augustus_randomsplit => $augustus_randomsplit,
        convert_fathom2genbank => $convert_fathom2genbank,
        etraining     => $etraining,
        mpiexec       => $mpiexec,
        openmpi_ldpreload => $openmpi_ldpreload,
    );

    my $tt = Template->new;

    if ($ARGV{'--augustus-gm'}
        || ($ARGV{'--gm-db'} && -e $ARGV_snap_db . $ARGV_organism . '.hmm'
            && -e $ARGV_augustus_db . $ARGV_augustus_prefix . $ARGV_organism)){
    
        $tt->process(\$altrun_tt, \%vars, 'maker_run.sh') or say 'Oups';

    }

    else {

        #TODO add robustness if '/' lacking
        if ($ARGV{'--augustus-new'}
            && -e $ARGV_augustus_db . $ARGV_augustus_prefix . $ARGV_organism) {
            my $cmd = 'rm -rf ' . $ARGV_augustus_db . $ARGV_augustus_prefix
                . $ARGV_organism;
            system $cmd;
        }

        $tt->process(\$run1_tt, \%vars, 'maker_run1.sh')
            or say 'Oups, an error occured while generating the script of the first maker run';

        $tt->process(\$run2_tt, \%vars, 'maker_run2.sh')
            or say 'Oups, an error occured while generating the script of the second maker run';

        $tt->process(\$run3_tt, \%vars, 'maker_run3.sh')
            or say 'Oups, an error occured while generating the script of the third maker run';
    }
}

sub maker_templating {

    my $opts_tt = <<'EOT';
#-----Genome (these are always required)
genome=[% genome %] #genome sequence (fasta file or fasta embeded in GFF3 file)
organism_type=[% org_type %] #eukaryotic or prokaryotic. Default is eukaryotic

#-----Re-annotation Using MAKER Derived GFF3
maker_gff= #MAKER derived GFF3 file
est_pass=0 #use ESTs in maker_gff: 1 = yes, 0 = no
altest_pass=0 #use alternate organism ESTs in maker_gff: 1 = yes, 0 = no
protein_pass=0 #use protein alignments in maker_gff: 1 = yes, 0 = no
rm_pass=0 #use repeats in maker_gff: 1 = yes, 0 = no
model_pass=0 #use gene models in maker_gff: 1 = yes, 0 = no
pred_pass=0 #use ab-initio predictions in maker_gff: 1 = yes, 0 = no
other_pass=0 #passthrough anyything else in maker_gff: 1 = yes, 0 = no

#-----EST Evidence (for best results provide a file for at least one)
est=[% transcripts %] #set of ESTs or assembled mRNA-seq in fasta format
altest= #EST/cDNA sequence file in fasta format from an alternate organism
est_gff= #aligned ESTs or mRNA-seq from an external GFF3 file
altest_gff= #aligned ESTs from a closly relate species in GFF3 format

#-----Protein Homology Evidence (for best results provide a file for at least one)
protein=[% proteins %]  #protein sequence file in fasta format (i.e. from mutiple organisms)
protein_gff=  #aligned protein homology evidence from an external GFF3 file

#-----Repeat Masking (leave values blank to skip repeat masking)
model_org=[%- IF masking == 1 %]all[%- END %] #select a model organism for RepBase masking in RepeatMasker
rmlib= #provide an organism specific repeat library in fasta format for RepeatMasker
repeat_protein=[%- IF masking %][% maskingDB %][%- END %] #provide a fasta file of transposable element proteins for RepeatRunner
rm_gff= #pre-identified repeat elements from an external GFF3 file
prok_rm=0 #forces MAKER to repeatmask prokaryotes (no reason to change this), 1 = yes, 0 = no
softmask=[% soft_mask %] #use soft-masking rather than hard-masking in BLAST (i.e. seg and dust filtering)

#-----Gene Prediction
snaphmm= [% snap_gm %]#SNAP HMM file
gmhmm= #GeneMark HMM file
augustus_species= [% augustus_gm %] #Augustus gene prediction species model
fgenesh_par_file= #FGENESH parameter file
pred_gff= #ab-initio predictions from an external GFF3 file
model_gff= #annotated gene models from an external GFF3 file (annotation pass-through)
[%- IF version3 %]run_evm=0 #run EvidenceModeler, 1 = yes, 0 = no[%- END %]
est2genome=[% est_ev  %] #infer gene predictions directly from ESTs, 1 = yes, 0 = no
protein2genome=[% prot_ev %] #infer predictions from protein homology, 1 = yes, 0 = no
trna=0 #find tRNAs with tRNAscan, 1 = yes, 0 = no
snoscan_rrna= #rRNA file to have Snoscan find snoRNAs
[%- IF version3 %]snoscan_meth= #-O-methylation site fileto have Snoscan find snoRNAs[%- END %]
unmask=[% IF masking == '1' %]0[% ELSE %]1[% END %] #also run ab-initio prediction programs on unmasked sequence, 1 = yes, 0 = no

#-----Other Annotation Feature Types (features MAKER doesn't recognize)
other_gff= #extra features to pass-through to final MAKER generated GFF3 file

#-----External Application Behavior Options
alt_peptide=C #amino acid used to replace non-standard amino acids in BLAST databases
cpus=1 #max number of cpus to use in BLAST and RepeatMasker (not for MPI, leave 1 when using MPI)

#-----MAKER Behavior Options
max_dna_len=[% split_length %] #length for dividing up contigs into chunks (increases/decreases memory usage)
min_contig=1 #skip genome contigs below this length (under 10kb are often useless)

pred_flank=[% pred_flank %] #flank for extending evidence clusters sent to gene predictors
pred_stats=[% pred_stats %] #report AED and QI statistics for all predictions as well as models
AED_threshold=1 #Maximum Annotation Edit Distance allowed (bound by 0 and 1)
min_protein=0 #require at least this many amino acids in predicted proteins
alt_splice=0 #Take extra steps to try and find alternative splicing, 1 = yes, 0 = no
always_complete=0 #extra steps to force start and stop codons, 1 = yes, 0 = no
map_forward=0 #map names and attributes forward from old GFF3 genes, 1 = yes, 0 = no
keep_preds=0 #Concordance threshold to add unsupported gene prediction (bound by 0 and 1)

split_hit=10000 #length for the splitting of hits (expected max intron size for evidence alignments)
[%- IF version3 %]min_intron=[% min_intron %] #minimum intron length (used for alignment polishing)[%- END %]
single_exon=0 #consider single exon EST evidence when generating annotations, 1 = yes, 0 = no
single_length=250 #min length required for single exon ESTs if 'single_exon is enabled'
correct_est_fusion=0 #limits use of ESTs in annotation to avoid fusion genes

tries=2 #number of times to try a contig if there is a failure for some reason
clean_try=0 #remove all data from previous run before retrying, 1 = yes, 0 = no
clean_up=0 #removes theVoid directory with individual analysis files, 1 = yes, 0 = no
TMP= #specify a directory other than the system default temporary directory for temporary files
EOT
    
    my $bopts_tt = <<'EOT';
#-----BLAST and Exonerate Statistics Thresholds
blast_type=[% blast_type %] #set to 'ncbi+', 'ncbi' or 'wublast'
[%- IF version3 %]use_rapsearch=0 #use rapsearch instead of blastx, 1 = yes, 0 = no[%- END %]

pcov_blastn=[% blastn_cov %] #Blastn Percent Coverage Threhold EST-Genome Alignments
pid_blastn=[% blastn_id %] #Blastn Percent Identity Threshold EST-Genome Aligments
eval_blastn=[% blastn_eval %] #Blastn eval cutoff
bit_blastn=40 #Blastn bit cutoff
depth_blastn=[% blastn_depth %] #Blastn depth cutoff (0 to disable cutoff)

pcov_blastx=[% blastx_cov %] #Blastx Percent Coverage Threhold Protein-Genome Alignments
pid_blastx=[% blastx_id %] #Blastx Percent Identity Threshold Protein-Genome Aligments
eval_blastx=[% blastx_eval %] #Blastx eval cutoff
bit_blastx=30 #Blastx bit cutoff
depth_blastx=[% blastx_depth %] #Blastx depth cutoff (0 to disable cutoff)

pcov_tblastx=0.8 #tBlastx Percent Coverage Threhold alt-EST-Genome Alignments
pid_tblastx=0.85 #tBlastx Percent Identity Threshold alt-EST-Genome Aligments
eval_tblastx=1e-10 #tBlastx eval cutoff
bit_tblastx=40 #tBlastx bit cutoff
depth_tblastx=0 #tBlastx depth cutoff (0 to disable cutoff)

pcov_rm_blastx=0.5 #Blastx Percent Coverage Threhold For Transposable Element Masking
pid_rm_blastx=0.4 #Blastx Percent Identity Threshold For Transposbale Element Masking
eval_rm_blastx=1e-06 #Blastx eval cutoff for transposable element masking
bit_rm_blastx=30 #Blastx bit cutoff for transposable element masking

ep_score_limit=20 #Exonerate protein percent of maximal score threshold
en_score_limit=20 #Exonerate nucleotide percent of maximal score threshold
EOT

    my $evm_tt = <<'EOT';
#-----Transcript weights
evmtrans=10 #default weight for source unspecified est/alt_est alignments
evmtrans:blastn=0 #weight for blastn sourced alignments
evmtrans:est2genome=10 #weight for est2genome sourced alignments
evmtrans:tblastx=0 #weight for tblastx sourced alignments
evmtrans:cdna2genome=7 #weight for cdna2genome sourced alignments

#-----Protein weights
evmprot=10 #default weight for source unspecified protein alignments
evmprot:blastx=2 #weight for blastx sourced alignments
evmprot:protein2genome=10 #weight for protein2genome sourced alignments

#-----Abinitio Prediction weights
evmab=10 #default weight for source unspecified ab initio predictions
evmab:snap=10 #weight for snap sourced predictions
evmab:augustus=10 #weight for augustus sourced predictions
evmab:fgenesh=10 #weight for fgenesh sourced predictions
evmab:genemark=7 #weight for genemark sourced predictions
EOT

    my $exe_tt = <<'EOT';
#-----Location of Executables Used by MAKER/EVALUATOR
makeblastdb=[% makeblastdb %] #location of NCBI+ makeblastdb executable
blastn=[% blastn %] #location of NCBI+ blastn executable
blastx=[% blastx %] #location of NCBI+ blastx executable
tblastx=[% tblastx %] #location of NCBI+ tblastx executable
formatdb= #location of NCBI formatdb executable
blastall= #location of NCBI blastall executable
xdformat= #location of WUBLAST xdformat executable
blasta= #location of WUBLAST blasta executable
[%- IF version3 %]prerapsearch= #location of prerapsearch executable[%- END %]
[%- IF version3 %]rapsearch= #location of rapsearch executable[%- END %]
RepeatMasker=[% repeatmasker %] #location of RepeatMasker executable
exonerate=[% exonerate %] #location of exonerate executable

#-----Ab-initio Gene Prediction Algorithms
snap=[% snap %] #location of snap executable
gmhmme3= #location of eukaryotic genemark executable
gmhmmp= #location of prokaryotic genemark executable
augustus=[% augustus %] #location of augustus executable
fgenesh= #location of fgenesh executable
[%- IF version3 %]evm= #location of EvidenceModeler executable[%- END %]
tRNAscan-SE= #location of trnascan executable
snoscan= #location of snoscan executable

#-----Other Algorithms
probuild= #location of probuild executable (required for genemark)
EOT

    my %vars_opts1 = (
        genome       => $ARGV_genome,
        org_type     => $ARGV_org_type,
        est_ev       => $ARGV_est,
        prot_ev      => $ARGV_proteins,
        transcripts  => $ARGV_est ? $ARGV_est_file : q//,
        proteins     => $ARGV_protein_file,
        masking      => $ARGV_masking,
        soft_mask    => $ARGV_soft_mask,
        snap_gm      => '',
        augustus_gm  => '',
        split_length => $ARGV_split_length,
        pred_flank   => $ARGV_pred_flank,
        pred_stats   => $ARGV_pred_stats,
        min_intron   => $ARGV_min_intron,
        maskingDB    => $maskingDB,
    );

    my %vars_opts2 = ( 
        genome       => $ARGV_genome,
        org_type     => $ARGV_org_type,
        est_ev       => 0,
        prot_ev      => 0,
        transcripts  => $ARGV_est ? $ARGV_est_file : q//,
        proteins     => $ARGV_protein_file,
        masking      => $ARGV_masking,
        soft_mask    => $ARGV_soft_mask,
        snap_gm      => 'gene_models/run1/snap/snap.hmm',
        augustus_gm  => '',
        split_length => $ARGV_split_length,
        pred_flank   => $ARGV_pred_flank,
        pred_stats   => $ARGV_pred_stats,
        min_intron   => $ARGV_min_intron,
        maskingDB    => $maskingDB,
    );

    my %vars_opts3 = ( 
        genome       => $ARGV_genome,
        org_type     => $ARGV_org_type,
        est_ev       => 0,
        prot_ev      => 0,
        transcripts  => $ARGV_est ? $ARGV_est_file : q//,
        proteins     => $ARGV_protein_file,
        masking      => $ARGV_masking,
        soft_mask    => $ARGV_soft_mask,
        snap_gm      => 'gene_models/run2/snap/snap.hmm',
        augustus_gm  => $ARGV_augustus_prefix . $ARGV_organism,
        split_length => $ARGV_split_length,
        pred_flank   => $ARGV_pred_flank,
        pred_stats   => $ARGV_pred_stats,
        min_intron   => $ARGV_min_intron,
        maskingDB    => $maskingDB,
    );

    my %vars_bopts = (
        blast_type   => $ARGV_blast_type,
        blastn_cov   => $ARGV_blastn_cov,
        blastn_id    => $ARGV_blastn_id,
        blastn_eval  => $ARGV_blastn_eval,
        blastn_depth => $ARGV_blastn_depth,
        blastx_cov   => $ARGV_blastx_cov,
        blastx_id    => $ARGV_blastx_id,
        blastx_eval  => $ARGV_blastx_eval,
        blastx_depth => $ARGV_blastx_depth,
        makeblastdb  => $makeblastdb,
        blastn       => $blastn,
        blastx       => $blastx,
        tblastx      => $tblastx,
        repeatmasker => $repeatmasker,
        exonerate    => $exonerate,
        snap         => $snap,
        augustus     => $augustus,
    );

    # fill-in template
    my $tt = Template->new;

    if ($ARGV_gm_db && -e $ARGV_snap_db . $ARGV_organism . '.hmm'
            && -e $ARGV_augustus_db . $ARGV_augustus_prefix . $ARGV_organism) {
    
    my %vars_altopts = (
        genome       => $ARGV_genome,
        org_type     => $ARGV_org_type,
        est_ev       => 0,
        prot_ev      => 0,
        transcripts  => $ARGV_est ? $ARGV_est_file : q//,
        proteins     => $ARGV_protein_file,
        masking      => $ARGV_masking,
        soft_mask    => $ARGV_soft_mask,
        snap_gm      => $ARGV_snap_db . $ARGV_organism . '.hmm',
        augustus_gm  => $ARGV_augustus_prefix . $ARGV_organism,
        split_length => $ARGV_split_length,
        pred_flank   => $ARGV_pred_flank,
        pred_stats   => $ARGV_pred_stats,
        min_intron   => $ARGV_min_intron,
        maskingDB    => $maskingDB,
    );


        $tt->process(\$opts_tt,  \%vars_altopts, 'maker_opts.ctl')
            or say "Oups, maker_opts.ctl cannot be generated.";

    }
    
    elsif ($ARGV_augustus_gm) {
    
        my %vars_altopts = (
            genome       => $ARGV_genome,
            org_type     => $ARGV_org_type,
            est_ev       => 0,
            prot_ev      => 0,
            transcripts  => $ARGV_est ? $ARGV_est_file : q//,
            proteins     => $ARGV_protein_file,
            masking      => $ARGV_masking,
            soft_mask    => $ARGV_soft_mask,
            snap_gm      => '',
            augustus_gm  => $ARGV_augustus_gm,
            split_length => $ARGV_split_length,
            pred_flank   => $ARGV_pred_flank,
            pred_stats   => $ARGV_pred_stats,
            min_intron   => $ARGV_min_intron,
        );

        $tt->process(\$opts_tt,  \%vars_altopts, 'maker_opts.ctl')
            or say "Oups, maker_opts.ctl cannot be generated.";

    }

    else {

    $tt->process(\$opts_tt,  \%vars_opts1, 'maker_opts.ctl')
        or say "Oups, maker_opts_run1.ctl cannot be generated.";

    $tt->process(\$opts_tt,  \%vars_opts2, 'maker_opts_run2.ctl')
        or say "Oups, maker_opts_run2.ctl cannot be generated.";

    $tt->process(\$opts_tt,  \%vars_opts3, 'maker_opts_run3.ctl')
        or say "Oups, maker_opts_run3.ctl cannot be generated.";

    }

    $tt->process(\$bopts_tt, \%vars_bopts, 'maker_bopts.ctl')
        or say "Oups, maker_bopts.ctl cannot be generated.";

    $tt->process(\$exe_tt,   \%vars_bopts, 'maker_exe.ctl')
        or say "Oups, maker_exe.ctl cannot be generated.";

    if ($ARGV_version3) {
        $tt->process(\$evm_tt,   \%vars_bopts, 'maker_evm.ctl')
            or say "Oups, maker_evm.ctl cannot be generated.";
    }
}

sub transcript_builder {

    ### Search of available SRAs data    
   
    # TODO option to pass SRA searching and filtering to give user SRAs id 

    my $eutils_base = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/';
    my $db = 'SRA';
    my $esearch = $eutils_base."esearch.fcgi?" . "db=$db&retmax=10000&term=";
    my $esummary = $eutils_base."esummary.fcgi?" .  "db=$db&id=";
    
    my $query = $ARGV_organism;

    my $url =  $esearch . $query . '[organism]' ;

    my $infile = 'esearch_result';
    system("wget -O $infile '$url'");

    my $xml_search = XML::Bare->new( file => $infile )
        or die "Can't open '$infile' for reading: $!";

    my $root_search = $xml_search->parse->{'eSearchResult'};

    my $IdList = $root_search->{IdList}->{Id};
    
    # cleaning downloaded files
    system 'rm -f esearch_result efetch_result';

    unless ($IdList) {
        # TODO stop the job here and prevent the user or keep on? 
        say 'No SRA from transcriptomic source available... No transcript '
            . 'evidence will be used for the genome annotation unless you add '
            . 'some experimental data with the --est-file option.'; 
        $ARGV_est = 0; 
        $rna_launch = 0;
        return;
    }

    else {
        $IdList = [$IdList] unless ref $IdList eq 'ARRAY';      # uniformise the use of $IdList
    }

    my %sra_for;
    
    my @ids = map { $_->{value} } @$IdList;

    say '# Number of available SRAs for ' .  $query . ': ' . scalar @ids;

    ### Analysis of the SRAs found... this can take some time
   
    my $storage;
    for my $id (@ids) {
        $sra_for{$query}{$id} = doFetch($id, $storage);
    }

    my @sra_lvl1 = grep { $_->{type} eq 'Illumina-pe' }
        @{ $sra_for{$query} }{@ids};
    my @sra_lvl2 = grep { $_->{type} eq 'Illumina-se' }
        @{ $sra_for{$query} }{@ids};
    my @sra_lvl3 = grep { $_->{type} eq 'other-pe'    }
        @{ $sra_for{$query} }{@ids};
    my @sra_lvl4 = grep { $_->{type} eq 'other-se'    }
        @{ $sra_for{$query} }{@ids};


    if (@sra_lvl1) {

        say '# There is ' . scalar @sra_lvl1 . ' available SRA from '
            . 'transcriptomic source, Illumina sequencing and paired-end';
        
        for my $id (@sra_lvl1) {
            say '# ' . $id->{'org'} . ' - ' . $id->{'bioproject'};
            say join "\n", @{ $id->{'sra'} };
        }

    }

    elsif (@sra_lvl2) {
       
        say '# There is ' . scalar @sra_lvl2 . ' available SRA from '
            . 'transcriptomic source, Illumina sequencing and single-end';
     
        for my $id (@sra_lvl2) {
            say '# ' . $id->{'org'} . ' - ' . $id->{'bioproject'};
            say join "\n", @{ $id->{'sra'} };
        }

    }
    
    elsif (@sra_lvl3) {
        
        say '# There is ' . scalar @sra_lvl3 . ' available SRA from '
            . 'transcriptomic source, 454 sequencing and paired-end';
     
        for my $id (@sra_lvl3) {
            say '# ' . $id->{'org'} . ' - ' . $id->{'bioproject'};
            say join "\n", @{ $id->{'sra'} };
        }

    }

    elsif (@sra_lvl4) {
        
        say '# There is ' . scalar @sra_lvl3 . ' available SRA from '
            . 'transcriptomic source, 454 sequencing and single-end';
     
        for my $id (@sra_lvl4) {
            say '# ' . $id->{'org'} . ' - ' . $id->{'bioproject'};
            say join "\n", @{ $id->{'sra'} };
        }

    }

    else {
        say 'No SRA from transcriptomic source available... no transcript '
            . 'evidence will be used for this run of annotation, if you still '
            . 'want to use some, run back this program with --est-file and '
            . 'experimental data of your own'
        ;

        $ARGV_est = 0;      # avoid use of transcriptomic data in MAKER runs.
        $rna_launch = 0;    # avoid the launching of transcriptomic jobs
        return;
    }

    my %vars_for;
    my @SRA_hashes
        = @sra_lvl1
        ? @sra_lvl1
        : @sra_lvl2
        ? @sra_lvl2
        : @sra_lvl3
        ? @sra_lvl3
        : @sra_lvl4
        ? @sra_lvl4
        : ()
    ;

    if (@SRA_hashes) {

        # take the SRAs presenting the majority orientation
        my $read_sum;
        map { $read_sum += $_->{read_n} } @SRA_hashes;
        
        #TODO adapt max value depending on selected queue
        my $memory = ceil( $read_sum / 1000000 );
        $memory = 100 if $memory > 100; # RAM on durandal: 128/164G8 for 32/40 cpus smallnodes, bignode and gpunode ?

        $ARGV_trinity_memory = $memory unless $ARGV_trinity_memory;
        
        my %count_for;
        for my $sra (@SRA_hashes) {
            $count_for{$sra->{orientation}}++ if defined $sra->{orientation};
        }

        my $consensus = max_by { $count_for{$_} } keys %count_for;

        my @SRAs = map { @{ $_->{sra} } } 
            grep { $_->{orientation} eq $consensus } @SRA_hashes;
        my $type = (map { $_->{type} } @SRA_hashes)[0];

        #TODO add an option to include single-end reads with paired-end (if less than x p-e?): the s-e reads have to be put in --left or --right depending of their orientation and L-R orientation

        say '# ' . scalar @SRAs . ' SRAs of the majoritary orientation ('
            . $consensus . ') used for the job templating'; 

        %vars_for = (
            queue       => $ARGV_queue,
            mail        => $ARGV_email,
            jobname     => $ARGV_jobname,
            type        => $type,
            level       => @sra_lvl1
                ? '1'
                : @sra_lvl2
                ? '2'
                : @sra_lvl3
                ? '1'
                : @sra_lvl4
                ? '2'
                : undef,
            sra          => \@SRAs,
            orientation  => $consensus,
            maker_cpus   => $ARGV_maker_cpus,
            trinity_cpus => $ARGV_trinity_cpus,
            outdir       => $ARGV_outdir,
            memory       => $ARGV_trinity_memory,
            rsem_cpus    => $ARGV_rsem_cpus,
            fastq_dump   => $fastq_dump,
            perl         => $perl,
            trinity      => $trinity,
            trinity_rsem => $trinity_rsem,
            transcript_filter => $transcript_filter,
        );
    }

    # templates   
    my $fastq_tt =  <<'EOT';
#!/bin/bash
#$ -V
#$ -cwd
#$ -q [% queue %]
#$ -m [% IF mail %]beas[% ELSE %]n[% END %]
[%- IF mail %]#$ -M [% mail %][% END %]
#$ -N [% jobname %]_fastq-dump

# [%- type %]
[% fastq_dump %] --defline-seq '@$sn[_$rn]/$ri' --split-files [% sra.join(" ") %]
EOT

    my $trinity_tt = <<'EOT';
#!/bin/bash
#$ -V
#$ -cwd
#$ -q [% queue %]
#$ -m [% IF mail %]beas[% ELSE %]n[% END %]
[%- IF mail %]#$ -M [% mail %][% END %]
#$ -N [% jobname %]_trinity

[%- IF level == 1 %] 
# Fix FASTQ deflines for Trinity v2.4.0
for forwardReads in [% sra.join("_1.fastq ") %]_1.fastq; do
    [% perl %] -i -ple 's/_forward//' $forwardReads
done 

for reverseReads in [% sra.join("_2.fastq ") %]_2.fastq; do
    [% perl %] -i -ple 's/_reverse//' $reverseReads
done 

# Run Trinity
[% trinity %] --seqType fq --left [% sra.join("_1.fastq,") %]_1.fastq --right [% sra.join("_2.fastq,") %]_2.fastq --min_kmer_cov 1 [% IF orientation %]--SS_lib_type [% orientation %][% END %] --trimmomatic [% IF memory %]--max_memory [% memory %]G[% END %] --CPU [% trinity_cpus %] --output [% outdir %]/trinity_out_dir
[%- END %]

[%- IF level == 2 %]
# Fix FASTQ deflines for Trinity v2.4.0
for forwardReads in [% sra.join("_1.fastq ") %]_1.fastq; do
    [% perl %] -i -ple 's/_forward//' $forwardReads
done 

# Run Trinity
[% trinity %] --seqType fq --single [% sra.join(".fastq,") %].fastq --min_kmer_cov 1 [% IF orientation %]--SS_lib_type [% orientation %][% END %] --trimmomatic [% IF memory %]--max_memory [% memory %]G[% END %] --CPU [% trinity_cpus %] --output [% outdir %]/trinity_out_dir
[%- END %]
EOT


    my $rsem_tt = <<'EOT';
#!/bin/bash
#$ -V
#$ -cwd
#$ -q [% queue %]
#$ -m [% IF mail %]beas[% ELSE %]n[% END %]
[%- IF mail %]#$ -M [% mail %][% END %]
#$ -N [% jobname %]_rsem

[%- IF level == 1 %] 
[% trinity_rsem %] --transcripts [% outdir %]/trinity_out_dir/Trinity.fasta --seqType fq --left [% sra.join("_1.fastq,") %]_1.fastq --right [% sra.join("_2.fastq,") %]_2.fastq --trinity_mode --prep_reference --est_method RSEM --aln_method bowtie2 --thread_count [% rsem_cpus %] --output_dir [% outdir %]/rsem_output
[%- END %]

[%- IF level == 2 %] 
[% trinity_rsem %] --transcripts [% outdir %]/trinity_out_dir/Trinity.fasta --seqType fq --single [% sra.join(".fastq,") %].fastq --trinity_mode --prep_reference --est_method RSEM --aln_method bowtie2 --thread_count [% rsem_cpus %] --output_dir [% outdir %]/rsem_output
[%- END %]
EOT


my $transcript_filter_tt = <<'EOT';
#!/bin/bash
#$ -V
#$ -cwd
#$ -q [% queue %]
#$ -m [% IF mail %]beas[% ELSE %]n[% END %]
[%- IF mail %]#$ -M [% mail %][% END %]
#$ -N [% jobname %]_transcript_filter

[% transcript_filter %] --rsem [% outdir %]/rsem_output/RSEM.isoforms.results --fasta [% outdir %]/trinity_out_dir/Trinity.fasta --output [% outdir %]/Trinity_filtered.fasta --filter-minor 0.10
# cleaning
mv -t . [% outdir %]/rsem_output/RSEM.genes.results [% outdir %]/rsem_output/RSEM.isoforms.results 
rm -rf Trinity.fasta.* [% outdir %]/rsem_output/
EOT

    # templating
    my $tt = Template->new;

    $tt->process(\$fastq_tt, \%vars_for, 'fastq-dump.sh')
        or die 'Oups';

    $tt->process(\$trinity_tt, \%vars_for, 'trinity.sh')
        or die 'Oups';

    $tt->process(\$rsem_tt, \%vars_for, 'rsem.sh')
        or die 'Oups';

    $tt->process(\$transcript_filter_tt, \%vars_for, 'transcript_filter.sh')
        or die 'Oups';

    sub doFetch {
        
        my ($id, $storage) = shift;
       
        #TODO delete these 3 variables if they are already defined in a global scope
        my $db = 'SRA';
        my $eutils_base = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/';
        my $efetch = $eutils_base."efetch.fcgi?" . "db=$db&id=";

        system("wget -O efetch_result '$efetch . $id'");
            
        my $efetch_result = get( $efetch . $id );
        my $xml_fetch = XML::Bare->new( file => 'efetch_result');

        my $root_fetch = $xml_fetch->parse();
        my $node = $root_fetch->{EXPERIMENT_PACKAGE_SET}->{EXPERIMENT_PACKAGE}; # intermediate node
    
        # avoid ambiguous SRAs
        my @exits = (
            $node->{Pool}->{Member}, 
            $node->{SAMPLE},
            $node->{RUN_SET}->{RUN},
        );

        return { type => 'ambiguous SRA report' }
            if grep { ref $_ eq 'ARRAY' } @exits; 
        
        # stop taking reads if enough size 
        # TODO problem: apply size filter after read selection
        my $size = $node->{RUN_SET}->{RUN}->{size}->{value}; # in bytes #TODO debug: sometimes NOT A HASH REFERENCE
        # $storage += $size;
        # return if $storage > $ARGV_max_storage 

        # avoid pair-end transcripts with different sizes
        my $read_n_ref = $node->{RUN_SET}->{RUN}->{Statistics}->{Read};
        $read_n_ref = [$read_n_ref] unless ref $read_n_ref eq 'ARRAY'; 

        my @read_n = map { $_->{count}->{value} } @{ $read_n_ref };

        if ( scalar @read_n > 1 && $read_n[1] != 0) {
            return if $read_n[0] != $read_n[1];
        }

        my $platform = (sort keys %{$node->{EXPERIMENT}->{PLATFORM}} )[0];
        my $strategy = $node->{EXPERIMENT}->{DESIGN}->{LIBRARY_DESCRIPTOR}->{
            'LIBRARY_STRATEGY'}->{value};
        my $layout   = $node->{EXPERIMENT}->{DESIGN}->{LIBRARY_DESCRIPTOR}->{
            'LIBRARY_LAYOUT'};

        # my $org_name  = $node->{SAMPLE}->{SAMPLE_NAME}->{SCIENTIFIC_NAME}->{value};   # not robust, it is sometimes empty
        my $org_name = $node->{Pool}->{Member}->{organism}->{value};
        my $taxId = $node->{SAMPLE}->{SAMPLE_NAME}->{TAXON_ID}->{value};
        my $paired_ends  = $node->{EXPERIMENT}->{DESIGN}->{
            'LIBRARY_DESCRIPTOR'}->{LIBRARY_LAYOUT}->{PAIRED};
        my $nominal_length  = $node->{EXPERIMENT}->{DESIGN}->{
            'LIBRARY_DESCRIPTOR'}->{LIBRARY_LAYOUT}->{PAIRED}->{
                'NOMINAL_LENGTH'}->{value};     # nominal value = insert size of dna input
        my $source = $node->{EXPERIMENT}->{DESIGN}->{LIBRARY_DESCRIPTOR}->{
            'LIBRARY_SOURCE'}->{value};
        
        my $orientation_ref = $node->{EXPERIMENT}->{DESIGN}->{
            'SPOT_DESCRIPTOR'}->{SPOT_DECODE_SPEC}->{READ_SPEC};
        $orientation_ref = [$orientation_ref]
            unless ref $orientation_ref eq 'ARRAY';
        
        my $orientation = (join q{}, map { $_->{READ_TYPE}->{value} // q{} }
            @$orientation_ref);

        my %orientation_for = (
            ForwardReverse => 'FR',
            Forward => 'F',
            Reverse => 'R',
            ReverseForward => 'RF',)
        ;

        $orientation = $orientation_for{$orientation} unless $orientation eq ''; 

        my $bioproject_node = $node->{STUDY}->{IDENTIFIERS}->{EXTERNAL_ID};

        my $bioproject;
        if (ref $bioproject_node eq 'ARRAY') {
            $bioproject = join ',', map { $_->{value} } @$bioproject_node;
        }

        else {
            $bioproject = $bioproject_node->{value};
        }

        my @run_files;
        my $run_list = $node->{RUN_SET}->{RUN};
        if ( ref $run_list eq 'ARRAY' ) {
            for my $run ( @$run_list ) {
                push @run_files, $run->{IDENTIFIERS}->{PRIMARY_ID}->{value};
            }
        } else {
            push @run_files, $run_list->{IDENTIFIERS}->{PRIMARY_ID}->{value};
        }
       
        my %sra_for;

        unless ($source ne 'TRANSCRIPTOMIC') {
         
            %sra_for = (
                org         => $org_name,
                bioproject  => $bioproject,
                source      => $source,
                orientation => $orientation,
                platform    => $platform,
                sra         => \@run_files,
                read_n      => $read_n[0] != 0 ? $read_n[0] : $read_n[1],
            );
        }

        if ($platform eq 'ILLUMINA' && $source eq 'TRANSCRIPTOMIC'
            && $paired_ends && defined $orientation) {
            
            $sra_for{ type } = 'Illumina-pe';
            $sra_for{layout} = 'paired-end';
        }

        elsif ($platform eq 'ILLUMINA' && $source eq 'TRANSCRIPTOMIC' ) {

            $sra_for{ type } = 'Illumina-se';
            $sra_for{layout} = 'single-end';
        }


        elsif ($source eq 'TRANSCRIPTOMIC' && $paired_ends
            && defined $orientation) {

            $sra_for{ type } = 'other-pe';
            $sra_for{layout} = 'paired-end';
        }

        elsif ($source eq 'TRANSCRIPTOMIC') {

            $sra_for{ type } = 'other-se';
            $sra_for{layout} = 'single-end';
        }

        else {
            $sra_for{type} = 'no sra';
        }

        return \%sra_for;
    }
}


=head1 VERSION

This documentation refers to

=head1 USAGE

$0 [options] --genome --organism

=head1 REQUIRED ARGUMENTS

=over

=item --genome [=] <file>

Path to your genomic data.

=item --organism [=] <Genus_species> 

Name of your organism with the format Genus_species or Genus_species_strain
(for the augustus gene model name).

=item --taxdir [=] <dir>

Path to local mirror of the NCBI Taxonomy database,IF YOU USE --proteins,
THIS OPTION IS MANDATORY.

=back

=head1 OPTIONS

=over

=item --sge

Use SGE job templating

=item --version3

Enables additional MAKER3 options.

=item --mpi

Uses MPI parallelization.

=item --queue [=] <queue>

bignode.q, smallnodes.q or gpunode.q. [default: smallnodes.q]

=for Euclid:
    queue.type: string
    queue.default: 'smallnodes.q'

=item --email [=] <email>

Type your email address to get informations with the progress of the
different steps.

=for Euclid:
    email.type: string

=item --jobname [=] <jobname>

basename for your jobs, by default it will be your organism
namewith a unique id.

=for Euclid:
    jobname.type: string

=item --org-type [=] <org>

Eukaryotic or prokaryotic [Default: eukaryotic].

=for Euclid:
    org.type: string
    org.default: 'eukaryotic'

=item --est [=] <n>

Use of EST/RNA-seq evidence (boolean switch: 0/1) [default: 0].

=for Euclid:
    n.type: /[01]/
    n.default: '0'

=item --est-file [=] <file>

Use of a selected EST/RNA-seq file instead of downloading and assembling
SRA data from NCBI. 

=for Euclid:
    file.type: readable

=item --transcript-db [=] <path> 

Alternative way to use automatically transcript assemblies: select the path to
a folder where you put all your transcripts with this nomenclature:

Genus_species*.fasta.

If no transcript file of your database  matched your organism,
SRA search will be executed. 

=item --proteins [=] <n>

Use of protein evidence (boolean switch: 0/1), if activated,
--taxdir option is needed. [default: 1]

=for Euclid:
    n.type: /[01]/
    n.default: '0'

=item --protein-file [=] <file>

Use of a selected protein file instead of using the default protein database. 

=for Euclid:
    file.type: readable

=item --outdir [=] <dirname> 

Name of the output directory containing the results. [default: amaw_output]

=for Euclid:
    dirname.type: string
    dirname.default: 'amaw_output'

=item --maker-cpus [=] <n>

Number of cpus to use for maker runs. [default: 25]

=for Euclid:
    n.type: int
    n.default: 25

=item --trinity-cpus [=] <n>

Number of cpus to use for Trinity. By default, it will calculate the number
of cpus to use in function of the number of reads.

=for Euclid:
    n.type: int
    n.default: 20

=item --trinity-memory [=] <n>

Number of GB RAM of memory to use for Trinity. By default, this value is
calculated in function of number of reads (1GB of memory by million of
paired-end reads). 

This calculated value is limited to 100/?/?/ GB when
smallnodes/gpunode/bignode is selected, but can be overridden by user entry.  

=for Euclid:
    n.type: int

=item --rsem-cpus [=] <n>

Number of cpus to use for rsem. By default, it will calculate the number of
CPUs to use in function of the number of reads.

=for Euclid:
    n.type: int
    n.default: 10

=item --augustus-prefix [=] <name>

Prefix to recognize Augustus gene models create for a user or a project.
By default, none.

=for Euclid:
    name.type: string
    name.default: ''

=item --augustus-gm [=] <string>

Name of an already existing augustus gene model (see the list on the augustus
config/species/ folder). 

This option only launches one MAKER run directly using the augustus gene model.

=item --gm-db 

Enables the use of previous gene models avaible for snap and
augustus (needs --snap-db and --augustus-db options).

This option will only launch one MAKER run directly using the SNAP and augustus
gene models (named as the --organism option in the folders given with --snap-db
and augustus-db). 

=item --snap-db [=] <dir>

Path to snap gene model database (e.g. /home/snap-db/).

=item --augustus-db [=] <dir>

Path to augustus gene model database
(e.g. /media/# /apps/augustus-3.2.2/config/species/).

=item --augustus-new

This option removes the previous augustus gene model. For this give,
the programs need the directory with gene models (Needs --augustus-db option).

=item --masking [=] <n> 

Mask repetitive elements in the genome (strongly recommanded if the genome
sequences are not masked) (boolean switch: 0/1) [default: 1].

=item --soft-mask [=] <n>

Use soft-masking rather than hard-masking in BLAST (boolean switch: 0/1)
[default: 1].

=for Euclid:
    n.type = /[0,1]/
    n.default = '1'

=item --split-length [=] <length>

Length for dividing up contigs into chunks (increases/decreases memory usage)
[default: 100000].

=for Euclid:
    length.type: integer
    length.default: 100000

=item --pred-flank [=] <n>

Flank for extending evidence clusters sent to gene predictors

=for Euclid:
    n.type: integer
    n.default: 200

=item --min-intron [=] <n>

Minimum intron length (used for alignment polishing) [default: 20].

=for Euclid:
    n.type: integer
    n.default: 20

=item --pred-stats [=] <n>

Report AED and QI statistics for all predictions as well as models
(boolean switch: 0/1) [default: 1].

=for Euclid:
    n.type = /[0,1]/
    n.default = '1'

=item --blast-type [=] <type>

Set to 'ncbi+', 'ncbi' or 'wublast'.

=for Euclid:
    type.type: str
    type.default: 'ncbi+'

=item --blastn-cov [=] <percent>

Blastn Percent Coverage Threshold EST-Genome Alignments [default: 0.8].

=for Euclid:
    percent.type: number
    percent.default: 0.8

=item --blastn-id [=] <percent>

Blastn Percent Identity Threshold EST-Genome Alignments [default: 0.85].

=for Euclid:
    percent.type: number
    percent.default: 0.85

=item --blastn-eval [=] <eval>

Blastn e-value cutoff [default: 1e-10].

=for Euclid:
    eval.type: number
    eval.default: 1e-10

=item --blastn-depth [=] <n>

Number of BLAST alignments, per query, to be used for annotation. Low values
decreases the memory use and the runtime for large evidence datasets
[default: 0].

=for Euclid:
    n.type: 0+integer
    n.default:0

=item --blastx-cov [=] <percent>

Blastx Percent Coverage Threhold Protein-Genome Alignments [default: 0.5].

=for Euclid:
    percent.type: number
    percent.default: 0.5

=item --blastx-id [=] <percent>

Blastx Percent Identity Threhold Protein-Genome Alignments [default: 0.4].

=for Euclid:
    percent.type: number
    percent.default: 0.4

=item --blastx-eval [=] <eval>

Blastx e-value cutoff [default: 1e-06].

=for Euclid:
    eval.type: number
    eval.default: 1e-06

=item --blastx-depth [=] <n>

Number of BLAST alignments, per query, to be used for annotation. Low values
decreases the memory use and the runtime for large evidence datasets
[default: 0].

=for Euclid:
    n.type: 0+integer
    n.default:0

=item --prot-dbs [=] <directory_path>

Path to the protein databases for the different eukaryotic clades.

=item --version

=item --usage

=item --help

=item --man

print the usual manual

=back

=head1 AUTHOR

=head1 COPYRIGHT
