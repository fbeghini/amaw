#!/usr/bin/env perl

use Modern::Perl '2011';
use autodie;
use Smart::Comments '###';

use Getopt::Euclid qw(:vars);
use List::AllUtils qw(uniq reduce);

open my $in, '<', $ARGV_rsem;

my %isoform_for;
my %gene_for if $ARGV_most;


LINE:
while (my $line = <$in>) {

    next LINE if $line =~ m/ transcript_id /xms;

    my ($transcript_id, $gene_id, $length, $effective_length, 
        $expected_count,  $TPM,  $FPKM, $isoPct) 
        = split "\t", $line;

    $isoform_for{ '>' . $transcript_id} = {
            length  => $length,
            exp_cnt => $expected_count,
            isoPct => $isoPct,
    };

    if ($ARGV_most) {

        $gene_for{$gene_id}{'>' . $transcript_id} = {
            length  => $length,
            exp_cnt => $expected_count,
            isoPct => $isoPct,
            TPM     => $TPM,
            FPKM    => $FPKM,
        };

    }

};


### test: scalar keys %gene_for
# ### %isoform_for

open my $in2, '<', $ARGV_fasta;

my %seq_for;
my $seq;
my $seq_id;

LINE:
while (my $line = <$in2>) {

    chomp $line;

    # at each '>' char...
    if (substr($line, 0, 1) eq '>' ) {
        
        # add current seq to hash (if any)
        if ($seq) {
            $seq_for{$seq_id} = $seq;
            $seq = q{} ;
        }

    # extract new seq_id
    ($seq_id) = $line =~ m/(>[^\s]*) \s len/xms;

    next LINE;

    }

    # elongate current seq (seqs can be broken on several lines)
    $seq .= $line;
}

# add last seq to hash (if any)
$seq_for{$seq_id} = $seq if $seq;

### Initial number of sequences: scalar keys %isoform_for
# ### %seq_for

# filter transcripts

my @filtered_transcripts;
my $rm_count = 0;

if ($ARGV_most) {
  
   
   for my $gene (keys %gene_for) {
        
        my @most_transcripts;
        push @most_transcripts, key_with_highest_val( $gene_for{$gene} );
        
        push @filtered_transcripts, grep { $_ !~ @most_transcripts } keys %{ $gene_for{$gene} };
    }

    $rm_count = scalar @filtered_transcripts;
    ### Number of sequences removed by 'most filter': $rm_count 
}

else {

    if ($ARGV_filter_minor) {
        
        for my $key (keys %isoform_for) {
            push @filtered_transcripts, $key
                if $isoform_for{$key}{isoPct} < $ARGV_filter_minor;
        }

    $rm_count = scalar @filtered_transcripts;
    ### Number of sequences removed by 'filter-minor': $rm_count 
    }


    if ($ARGV_filter_ecount) {
       
        #TODO push @filtered_transcripts, grep { $isoform{$_}{exp_cnt} < $ARGV_filter_ecount;

        for my $key (keys %isoform_for) {
            push @filtered_transcripts, $key
                if $isoform_for{$key}{exp_cnt} < $ARGV_filter_ecount;
        }

        ### Number of sequences removed by 'filter-ecount':  (scalar @filtered_transcripts - $rm_count)
    }
}

### Total number of sequences removed: scalar uniq  @filtered_transcripts

### Total number of sequences in output: (scalar keys %isoform_for) - (scalar uniq  @filtered_transcripts)

if ($ARGV_output) {

    open my $out, '>', $ARGV_output;

    for my $key (keys %seq_for) {
        
        say {$out} $key . "\n" . $seq_for{$key} unless grep{ $key eq $_ } @filtered_transcripts;

    }

}


sub key_with_highest_val {
       my ($h) = @_;
        return reduce { $h->{$a}{$ARGV_most} <=> $h->{$b}{$ARGV_most} ? $a : $b } keys(%$h);
}


=head1 NAME

transcript-filter - This tool intends to clean up assembly transcripts based on the expected count of isoforms (obtained by RSEM).
    
=head1 VERSION

This documentation refers to transcript-filter version 0.0.1

=head1 USAGE

$0 [options] --rsem <path> --fasta <path>

=head1 REQUIRED ARGUMENTS

=over

=item --rsem <path>

Path to the RSEM file containing isoforms results of RSEM.

=item --fasta <path>

Path to the fasta file containing transcripts.

=back

=head1 OPTIONS

=over

=item --filter-minor <value>

Remove minor isoforms that are equally or less represented than a given percent isoform [between 0.00-100].

=for Euclid:
    value.type = 0+num

=item --filter-ecount <count>

Remove isoforms that are equally or less represented than a given expected count.

=for Euclid:
    count.type = 0+num

=item --most  <variable> 

Keep only for a gene the isoform with the highest value for a variable. If this option is selected, other filters are disabled. 
Variables available: isoPct, length, TPM, FPKM, exp_count.

=for Euclid:
    variable.type = str

=item --output <path>

If wanted, path to the filtered transcript fasta file to write (the writing part takes some time, you will probably want to know how many isoforms you filter before do this).

=item --version

=item --usage

=item --help

=item --man

print the usual program information

=back

=head1 AUTHOR

=head1 COPYRIGHT
